﻿using MemoLink.Data.SQLite.EntityFramework.Configuration;
using System.Data.Common;
using System.Data.Entity;

namespace MemoLink.Data.SQLite.EntityFramework
{

    public class MemoLinkDbContext : DbContext
    {
    //    static MemoLinkDbContext()
    //    {
    //        //DbConfiguration.SetConfiguration(new SQLiteConfiguration());
    //    }

    //    public static string CreateConnectionString(string filePath)
    //    {
    //        var connectionStringBuilder = new SQLiteConnectionStringBuilder();
    //        connectionStringBuilder.DataSource = filePath;
    //        connectionStringBuilder.Version = 3;
    //        return connectionStringBuilder.ToString();
    //    }

        static MemoLinkDbContext()
        {
            Database.SetInitializer<MemoLinkDbContext>(null);
        }

        //public MemoLinkDbContext(DbConnection dbConnection)
        //    : base(dbConnection, true)
        //{
        //    //Logger.Trace("AutoExchangeDbContext created with context with connection string: {0}", dbConnection.ConnectionString);
        //}

        public MemoLinkDbContext(string filePath)
            //: base(CreateConnectionString(filePath))
            : base(filePath)
        {
            //Logger.Trace("AutoExchangeDbContext created with connection string: {0}", connectionString);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new NodeConfiguration());
            modelBuilder.Configurations.Add(new TagConfiguration());
            modelBuilder.Configurations.Add(new SourceConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
