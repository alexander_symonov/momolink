﻿using MemoLink.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MemoLink.Data.SQLite.EntityFramework.Configuration
{
    public class SourceConfiguration : EntityTypeConfiguration<Source>
    {
        public SourceConfiguration()
        {
            HasKey(node => node.Id);
            Property(node => node.Id)
                .HasColumnName("ID")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            Property(x => x.Type).IsRequired();
            Property(x => x.Uri)
                .HasColumnName("Link")
                .HasMaxLength(1024)
                .IsRequired();

            Property(x => x.OwnerId)
                 .HasColumnName("OwnerID")
                 .IsRequired();

            ToTable("Source");
        }
    }
}