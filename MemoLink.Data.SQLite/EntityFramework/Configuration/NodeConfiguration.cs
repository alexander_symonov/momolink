﻿using MemoLink.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace MemoLink.Data.SQLite.EntityFramework.Configuration
{
    public class NodeConfiguration : EntityTypeConfiguration<Node>
    {
        public NodeConfiguration()
        {
            HasKey(node => node.Id);
            Property(node => node.Id)
                .HasColumnName("ID")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            Property(node => node.ParentId)
                .HasColumnName("ParentID")
                .IsOptional();

            Property(node => node.RootId)
                .HasColumnName("RootId")
                .IsOptional();

            Property(node => node.SourceId)
                .HasColumnName("SourceId")
                .IsOptional();

            Property(node => node.Anchor)
               .HasColumnName("Anchor")
               .IsOptional()
               .HasMaxLength(128);

            Property(node => node.Title)
                .HasColumnName("Title")
                .IsRequired()
                .HasMaxLength(128);

            Property(node => node.Description)
                .HasColumnName("Description")
                .IsRequired()
                .HasMaxLength(1024);

            Property(node => node.Enabled)
                .HasColumnName("Enabled")
                .IsRequired();

            Property(node => node.Shared)
                .HasColumnName("Shared")
                .IsRequired();

            HasMany(x => x.Tags)
                .WithMany(x => x.Nodes)
                .Map(x => x
                    .MapLeftKey("NodeID")
                    .MapRightKey("TagID")
                    .ToTable("NodeTag"));

            //HasMany(currency => currency.Rates)
            //    .WithRequired(rate => rate.SourceCurrency)
            //    .HasForeignKey(rate => rate.SourceCurrencyId);

            HasOptional(node => node.Root);
                //.WithMany(bundle => bundle.Nodes);

            HasOptional(node => node.Parent)
                .WithMany(bundle => bundle.Nodes);

            HasOptional(node => node.Source);

            ToTable("Node");
        }
    }
}