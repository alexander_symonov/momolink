﻿using MemoLink.Domain.Entities;
using MemoLink.Domain.Repositories;
using MemoLink.Domain.Services;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace MemoLink.Data.SQLite.Services
{
    public class NodeInfoService : INodeInfoService
    {
        private readonly IRepository<Node> _nodeRepository;

        public NodeInfoService(IRepository<Node> nodeRepository)
        {
            _nodeRepository = nodeRepository;
        }

        public IEnumerable<DTO.NodeInfo> GetRootList()
        {
            return _nodeRepository.Filter(x => !x.RootId.HasValue)
                .Include("Tags")
                .Include("Nodes")
                .Include("Source")
                //.Include("Anchor")
                .ToList()
                .Select(x => ToDto(x));
        }

        public IEnumerable<DTO.NodeInfo> GetChildsInfo(int nodeId)
        {
            return _nodeRepository.Filter(x => x.ParentId == nodeId)
                .Include("Tags")
                .Include("Nodes")
                .Include("Source")
                //.Include("Anchor")
                .ToList()
                .Select(x => ToDto(x));
        }

        private DTO.NodeInfo ToDto(Node node)
        {
            return new DTO.NodeInfo()
            {
                Id = node.Id,
                Title = node.Title,
                NodesCount = node.Nodes.Count(),
                Tags = node.Tags
                    .Select(x => x.Name),
                    //.Select(t => new DTO.Tag()
                    //{
                    //    Id = t.Id,
                    //    Name = t.Name
                    //}),
                Anchor = node.Anchor,
                Description = node.Description,
                Source = new DTO.Source()
                {
                    Type = node.Source.Type,
                    Uri = node.Source.Uri
                }
            };
        }
    }
}