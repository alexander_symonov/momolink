﻿using MemoLink.Domain.Entities;
using MemoLink.Domain.Repositories;
using MemoLink.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MemoLink.Data.SQLite.Services
{
    public class TagService : ITagService
    {
        private readonly IRepository<Tag> _tagRepository;

        public TagService(IRepository<Tag> tagRepository)
        {
            _tagRepository = tagRepository;
        }

        public int Clear()
        {
            throw new NotImplementedException();
        }

        public int GetTagId(string name)
        {
            var tag = _tagRepository.Find(x => x.Name == name)
                ?? _tagRepository.Create(new Tag() { Name = name });

            return tag.Id;
        }

        public IEnumerable<int> GetTagsIds(List<string> names)
        {
            var existsTags = _tagRepository.Filter(x => names.Contains(x.Name));
            var result = existsTags.Select(x => x.Id).ToList();

            foreach (var name in names.Except(existsTags.Select(x => x.Name)))
            {
                var tag = _tagRepository.Create(new Tag() { Name = name });
                result.Add(tag.Id);
            }

            return result;
        }
    }
}