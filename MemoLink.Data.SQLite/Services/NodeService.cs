﻿using MemoLink.Domain.Entities;
using MemoLink.Domain.Repositories;
using MemoLink.Domain.Services;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace MemoLink.Data.SQLite.Services
{
    public class NodeService : INodeService
    {
        private readonly IRepository<Node> _nodeRepository;
        private readonly IRepository<Source> _sourceRepository;
        private readonly IRepository<Tag> _tagRepository;

        public NodeService(IRepository<Node> nodeRepository, IRepository<Source> sourceRepository, IRepository<Tag> tagRepository)
        {
            _nodeRepository = nodeRepository;
            _sourceRepository = sourceRepository;
            _tagRepository = tagRepository;
        }

        public int CreateRoot(string title, string description, DTO.Source source)
        {
            var newSource = _sourceRepository.Create(new Source()
            {
                Type = source.Type,
                Uri = source.Uri
            });

            var newItem = new Node()
            {
                Title = title,
                Description = description,
                Source = newSource
            };

            return _nodeRepository.Create(newItem).Id;
        }

        public int Create(int rootId, int? parentId, string title, string description, string anchor, IEnumerable<string> tags)
        {
            var rootNode = _nodeRepository.Find(rootId);

            var newItem = new Node()
            {
                Anchor = anchor,
                Title = title,
                Description = description,
                RootId = rootId,
                ParentId = parentId,
                SourceId = rootNode.SourceId
            };

            if (parentId.HasValue)
            {
                newItem.ParentId = parentId.Value;
            }

            //SaveNodeTags(newItem, tags);

            return _nodeRepository.Create(newItem).Id;
        }

        public void SaveData(int id, string title, string description, string anchor, IEnumerable<string> tags)
        {
            var node = _nodeRepository.Filter(x => x.Id == id).Include("Tags").Single();//.Find(id);
            node.Title = title;
            node.Description = description;
            node.Anchor = anchor;

            SaveNodeTags(node, tags);

            _nodeRepository.SaveChanges();
        }

        private void SaveNodeTags(Node node, IEnumerable<string> tags)
        {
            var existingTags = node.Tags.ToList();
            var existingTagsNames = existingTags.Select(x => x.Name).ToList();
            var notExistingTagsNames = tags.Except(existingTagsNames).ToList();

            // remove absent tags
            foreach (var nodeTag in existingTagsNames.Except(tags))
            {
                node.Tags.Remove(existingTags.Single(x => x.Name == nodeTag));
            }
            // Add existing tags
            foreach (var tag in _tagRepository.Filter(x => notExistingTagsNames.ToList().Contains(x.Name)))
            {
                notExistingTagsNames.Remove(tag.Name);
                node.Tags.Add(tag);
            }

            // create and add not existing tags
            foreach (var tagName in notExistingTagsNames)
            {
                var tag = _tagRepository.Create(new Tag { Name = tagName });
                node.Tags.Add(tag);
            }
        }

        /// <summary>
        /// TODO: check to remove
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public IEnumerable<DTO.Node> GetChildNodes(int parentId)
        {
            return _nodeRepository.Filter(x => x.ParentId == parentId)
                .Select(x => new DTO.Node()
                {
                    Id = x.Id,
                    Title = x.Title,
                    Tags = x.Tags.Select(t => t.Name),
                    Anchor = x.Anchor,
                    Description = x.Description,
                    Source = new DTO.Source()
                    {
                        Type = x.Source.Type,
                        Uri = x.Source.Uri
                    }
                });
        }

        public IEnumerable<DTO.Node> GetBundle(int bundleId)
        {
            var nodes = _nodeRepository.Filter(x => x.RootId == bundleId || x.Id == bundleId)
                .Include("Tags").Include("Source")
                .ToList()
                .Select(x => new { x.ParentId, instance = ToDtoNode(x) })
                .ToList();
            nodes.ForEach(x =>
            {
                x.instance.Nodes = nodes.Where(sub => sub.ParentId == x.instance.Id).Select(n => n.instance).ToList();
            });

            return nodes.Where(x => x.instance.Id == bundleId).Select(x => x.instance);
        }

        public IEnumerable<DTO.Node> GetBundleData(int bundleId)
        {
            var nodes = _nodeRepository.Filter(x => x.RootId == bundleId || x.Id == bundleId)
                .Include("Tags").Include("Source")
                .ToList()
                .Select(x => ToDtoNode(x))
                .ToList();

            return nodes;
        }

        public void Remove(int id)
        {
            // TODO Update. Clear sources or create disable flag
            _nodeRepository.Delete(x => x.Id == id);
            _nodeRepository.SaveChanges();
        }

        private DTO.Node ToDtoNode(Node node)
        {
            return new DTO.Node()
            {
                Id = node.Id,
                Title = node.Title,
                ParentId = node.ParentId,
                Tags = node.Tags
                    .Select(x => x.Name),
                    //.Select(t => new DTO.Tag()
                    //{
                    //    Id = t.Id,
                    //    Name = t.Name
                    //}),
                Anchor = node.Anchor,
                Description = node.Description,
                Source = new DTO.Source()
                {
                    Type = node.Source.Type,
                    Uri = node.Source.Uri,
                    OwnerId = node.Source.OwnerId
                }
            };
        }

        public void CloneSingle(int sourceId, int targetId)
        {
            throw new System.NotImplementedException();
        }

        public void CloneDeep(int sourceId, int targetId)
        {
            throw new System.NotImplementedException();
        }

        public void Move(int sourceId, int targetId)
        {
            throw new System.NotImplementedException();
        }
    }
}