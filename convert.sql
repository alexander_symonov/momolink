BEGIN TRANSACTION;
DROP TABLE IF EXISTS `Tag`;
CREATE TABLE IF NOT EXISTS `Tag` (
	`ID`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`Name`	nvarchar ( 50 ) NOT NULL
);
INSERT INTO `Tag` (ID,Name) VALUES (1,'qwe'),
 (2,'sdf'),
 (3,'sdff'),
 (4,'sdffs'),
 (5,'dsf'),
 (6,'dsfds'),
 (7,'dsfdss'),
 (8,'dsfdssdd'),
 (9,'qwe1'),
 (10,'qwe12'),
 (11,'as'),
 (12,'as1'),
 (13,'111'),
 (14,'222'),
 (15,'444'),
 (16,'4444'),
 (17,'333');
DROP TABLE IF EXISTS `Source`;
CREATE TABLE IF NOT EXISTS `Source` (
	`ID`	integer NOT NULL,
	`Type`	tinyint NOT NULL,
	`Link`	nvarchar ( 1024 ) NOT NULL,
	`OwnerID`	INT NOT NULL DEFAULT (0),
	PRIMARY KEY(`ID`)
);
INSERT INTO `Source` (ID,Type,Link,OwnerID) VALUES (1,1,'',0),
 (2,1,'',0),
 (3,1,'http://survinat.ru/category/article/food/#axzz3hrZ7vYZ2',0),
 (4,1,'http://insiderblogs.info/',0),
 (5,1,'',0);
DROP TABLE IF EXISTS `NodeTag`;
CREATE TABLE IF NOT EXISTS `NodeTag` (
	`NodeID`	integer NOT NULL,
	`TagID`	integer NOT NULL,
	CONSTRAINT `FK_NodeTag_Node` FOREIGN KEY(`NodeID`) REFERENCES `Node`(`ID`) MATCH NONE ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `PK_NodeTag` PRIMARY KEY(`NodeID`,`TagID`),
	CONSTRAINT `FK_NodeTag_Tag` FOREIGN KEY(`TagID`) REFERENCES `Tag`(`ID`) MATCH NONE ON UPDATE NO ACTION ON DELETE NO ACTION
);
INSERT INTO `NodeTag` (NodeID,TagID) VALUES (26,17),
 (26,13);
DROP TABLE IF EXISTS `Node`;
CREATE TABLE IF NOT EXISTS `Node` (
	`ID`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`RootID`	integer,
	`ParentID`	integer,
	`Title`	nvarchar ( 128 ) NOT NULL,
	`Description`	nvarchar ( 1024 ) NOT NULL DEFAULT '',
	`Enabled`	bit NOT NULL,
	`Shared`	bit NOT NULL,
	`Anchor`	nvarchar ( 128 ) DEFAULT '',
	`SourceID`	integer,
	CONSTRAINT `FK_Node_1_0` FOREIGN KEY(`ParentID`) REFERENCES `Node`(`ID`) MATCH NONE ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `FK_Node_0_0` FOREIGN KEY(`RootID`) REFERENCES `Node`(`ID`) MATCH NONE ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `FK_Node_-1_-1` FOREIGN KEY(`SourceID`) REFERENCES `Source`(`ID`)
);
INSERT INTO `Node` (ID,RootID,ParentID,Title,Description,Enabled,Shared,Anchor,SourceID) VALUES (18,NULL,NULL,'Secured','',0,0,NULL,1),
 (19,18,18,'Conspirology','',0,0,'',1),
 (20,18,19,'insiderblogs','',0,0,'http://insiderblogs.info/',1),
 (22,18,18,'Survive','',0,0,'',1),
 (23,18,22,'Food','',0,0,'',1),
 (24,18,23,'1','',0,0,'http://survinat.ru/category/article/food/#axzz3hrZ7vYZ2',1),
 (25,NULL,NULL,'eeee1','1',0,0,'1',2),
 (26,NULL,NULL,'1222','',0,0,'http://survinat.ru/category/article/food/#axzz3hrZ7vYZ2',3),
 (29,25,25,'1','',0,0,NULL,2),
 (30,25,25,'2','',0,0,NULL,2),
 (31,25,25,'3','',0,0,NULL,2),
 (32,25,25,'4','',0,0,NULL,2),
 (33,25,29,'vvvv','',0,0,'',2),
 (34,25,29,'fff','',0,0,'',2),
 (35,25,33,'gggg','',0,0,'',2),
 (36,25,33,'hhhh','',0,0,'',2),
 (37,25,29,'hhhh','',0,0,'',2),
 (38,25,33,'mmmm','',0,0,'',2),
 (39,25,25,'ggg','',0,0,'',2),
 (40,25,25,'jjjj1','',0,0,NULL,2),
 (41,25,39,'222','',0,0,'',2),
 (42,25,39,'22222','',0,0,'',2),
 (43,25,25,'vvv','',0,0,'',2),
 (44,25,NULL,'vvvbbbb','',0,0,'',2),
 (45,25,43,'111','',0,0,'',2),
 (46,25,NULL,'222','',0,0,'',2),
 (47,25,31,'vvvv','',0,0,'',2),
 (48,25,29,'eee','',0,0,'',2),
 (49,25,30,'1','',0,0,'',2),
 (50,25,30,'2','',0,0,'',2),
 (51,25,50,'23','',0,0,'',2),
 (52,25,32,'1','',0,0,'',2),
 (53,25,52,'11','',0,0,'',2),
 (54,25,32,'2','',0,0,'',2),
 (55,25,54,'22','',0,0,'',2),
 (56,25,55,'222','',0,0,'',2);
DROP TABLE IF EXISTS `Bundle`;
CREATE TABLE IF NOT EXISTS `Bundle` (
	`ID`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`Title`	nvarchar ( 128 ) NOT NULL,
	`Description`	nvarchar ( 1024 ) NOT NULL DEFAULT '',
	`Enabled`	bit NOT NULL,
	`Shared`	bit NOT NULL
);
INSERT INTO `Bundle` (ID,Title,Description,Enabled,Shared) VALUES (1,'111a','asdsad',1,0),
 (2,'121','sdfdf',0,0),
 (3,'123','sdf',0,0),
 (4,'3324','fddf',0,0);
COMMIT;
