﻿using MemoLink.Domain.Services;
using MemoLink.DTO;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MemoLink.Web.MVC5.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class NodeInfoController : ApiController
    {
        private INodeInfoService _nodeInfoService;
        public NodeInfoController(INodeInfoService nodeInfoService)
        {
            _nodeInfoService = nodeInfoService;
        }

        // GET: api/NodeInfo
        public IEnumerable<NodeInfo> Get()
        {
            return _nodeInfoService.GetRootList();
        }
       
        //// GET: api/NodeInfo/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST: api/NodeInfo
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT: api/NodeInfo/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/NodeInfo/5
        //public void Delete(int id)
        //{
        //}
    }
}
