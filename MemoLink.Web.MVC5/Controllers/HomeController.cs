﻿using MemoLink.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MemoLink.Web.MVC5.Controllers
{
    [OutputCache(Duration = 0)]
    public class HomeController : Controller
    {
        private INodeService _nodeService;
        public HomeController(INodeService nodeService)
        {
            _nodeService = nodeService;
        }
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        //public ActionResult BundleWindow(int id)
        //{
        //    var data = _nodeService.GetBundle(id);
        //    return PartialView("_bundleWindow", data);
        //}

        [OutputCache(Duration = 0)]
        public ActionResult BundleWindow(int id)
        {
            return PartialView("_bundleWindowNative", id);
        }
    }
}