﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MemoLink.Domain.Services;
using System.Linq;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace MemoLink.Web.MVC5.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DataController : Controller
    {
        private INodeInfoService _nodeInfoService;
        private INodeService _nodeService;
        public DataController(INodeInfoService nodeInfoService, INodeService nodeService)
        {
            _nodeInfoService = nodeInfoService;
            _nodeService = nodeService;
        }

        public JsonResult GetBundles([DataSourceRequest]DataSourceRequest request)
        {
            var result = _nodeInfoService.GetRootList();

            return Json(
                result.ToList().ToDataSourceResult(request, ModelState),
                JsonRequestBehavior.AllowGet);
        }

        //GET: Data
        public JsonResult GetNodeChilds(int rootId, int? id)
        {
            //var result =  _nodeService.GetBundle(id);
            var result = _nodeInfoService.GetChildsInfo(id ?? rootId);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBundle(int id)
        {
            var data = _nodeService.GetBundle(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

         public JsonResult GetBundleData(int id)
        {
            var data = _nodeService.GetBundleData(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}