﻿using MemoLink.Domain.Services;
using MemoLink.DTO;
using MemoLink.Web.MVC5.Models.NodeModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MemoLink.Web.MVC5.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class NodeController : ApiController
    {
        private INodeService _nodeService;

        public NodeController(INodeService nodeService)
        {
            _nodeService = nodeService;
        }

        // GET: api/Node/5
        public IEnumerable<Node> Get(int id)
        {
            return _nodeService.GetBundle(id);
        }

        // POST: api/Node
        public void Post([FromBody] UpdateNodeModel model)
        {
            if (!model.IsValid())
            {
                throw new ArgumentOutOfRangeException();
            }

            _nodeService.SaveData(
                model.Id,
                model.Title,
                model.Description ?? string.Empty,
                model.Anchor,
                model.Tags);
        }

        // PUT: api/Node/5
        public int Put([FromBody] CreateNodeModel model)
        {
            if (!model.IsValid())
            {
                throw new ArgumentOutOfRangeException();
            }

            return _nodeService.Create(
                model.RootId,
                model.ParentId,
                model.Title,
                model.Description ?? string.Empty,
                model.Anchor ?? string.Empty,
                model.Tags);
        }

        // DELETE: api/Node/5
        public void Delete(int id)
        {
            _nodeService.Remove(id);
        }

        // POST: api/Node/Clone
        [HttpPost]
        [Route("api/Node/Clone")]
        public void Clone([FromBody] CloneNodeModel model)
        {
            if (!model.IsValid())
            {
                throw new ArgumentOutOfRangeException();
            }

            switch (model.Type)
            {
                case CloneType.Cut:
                    _nodeService.Move(model.SourceId, model.TargetId);
                    break;
                case CloneType.Single:
                    _nodeService.CloneSingle(model.SourceId, model.TargetId);
                    break;
                case CloneType.Full:
                    _nodeService.CloneDeep(model.SourceId, model.TargetId);
                    break;
                default:
                    throw new InvalidEnumArgumentException(nameof(model.Type), (int) model.Type, model.Type.GetType());
            }
        }
    }
}