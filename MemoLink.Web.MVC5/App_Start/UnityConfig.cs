using MemoLink.Data.MsSql.EntityFramework;
using MemoLink.Data.MsSql.Repositories;
using MemoLink.Data.MsSql.Services;
using MemoLink.Domain.Entities;
using MemoLink.Domain.Repositories;
using MemoLink.Domain.Services;
using Microsoft.Practices.Unity;
using System.Web.Http;

namespace MemoLink.Web.MVC5
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<MemoLinkDbContext>(new HierarchicalLifetimeManager(),
                new InjectionConstructor("MemoLinkContext"));

            container.RegisterType<INodeInfoService, NodeInfoService>();
            container.RegisterType<INodeService, NodeService>();
            container.RegisterType<ITagService, TagService>();
            container.RegisterType<IRepository<Node>, Repository<Node>>();
            container.RegisterType<IRepository<Tag>, Repository<Tag>>();
            container.RegisterType<IRepository<Source>, Repository<Source>>();


            // MVC resolver
            System.Web.Mvc.DependencyResolver.SetResolver(new Unity.Mvc5.UnityDependencyResolver(container));

            // WEB.API resolver
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
        }
    }
}