﻿function onTreeNodeSelect(e) {
    var dataItem = this.dataItem(e.node);
    console.log(data.id);

}

function onTreeNodeChange(e) {
    selected = this.select(),
    item = this.dataItem(selected);
    this.set("selectedNode", item);
}

MemoLink = {};

MemoLink.EventManager = new function () {
    var events = {};
    this.on = function (evtName, callback) {
        if (!events[evtName]) events[evtName] = [];
        events[evtName].push(callback);
    };
    this.trigger = function (evtName, evtObj) {
        if (events[evtName]) {
            events[evtName].forEach(function (callback) {
                callback(evtObj);
            });
        }
    };
    this.remove = function (evtName) {
        delete events[evtName];
    };
};

MemoLink.alert = function (text) {
    alert(text);
}