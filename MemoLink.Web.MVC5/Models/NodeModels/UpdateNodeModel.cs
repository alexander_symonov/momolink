﻿namespace MemoLink.Web.MVC5.Models.NodeModels
{
    public class UpdateNodeModel : NodeModelBase
    {
        public int Id { get; set; }

        public override bool IsValid()
        {
            return base.IsValid() && Id != 0;
        }
    }
}