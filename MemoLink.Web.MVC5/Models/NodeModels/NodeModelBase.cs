﻿using System.Collections.Generic;

namespace MemoLink.Web.MVC5.Models.NodeModels
{
    public class NodeModelBase
    {
        public string Title { get; set; }

        public string Description { get; set; } = string.Empty;

        public string Anchor { get; set; } = string.Empty;

        public IEnumerable<string> Tags { get; set; } = new string[] { };

        public virtual bool IsValid()
        {
            return !string.IsNullOrEmpty(Title);
        }
    }
}