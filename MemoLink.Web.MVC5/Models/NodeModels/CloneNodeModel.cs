﻿namespace MemoLink.Web.MVC5.Models.NodeModels
{
    public enum CloneType
    {
        Default = 0,
        Single = 1,
        Full = 2,
        Cut = 3
    }

    public class CloneNodeModel
    {
        public CloneType Type { get; set; }

        public int SourceId { get; set; }

        public int TargetId { get; set; }

        public bool IsValid()
        {
            return Type != CloneType.Default
                   && SourceId != 0
                   && TargetId != 0;
        }
    }
}