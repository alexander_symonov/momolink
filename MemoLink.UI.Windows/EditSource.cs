﻿using System;
using System.Windows.Forms;

namespace MemoLink.UI.Windows
{
    public partial class EditSource : Form
    {
        public string Address { get; set; }

        public EditSource()
        {
            InitializeComponent();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            Address = textBoxAddress.Text;
            DialogResult = DialogResult.OK;
        }
    }
}
