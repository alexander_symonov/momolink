﻿using MemoLink.Data.SQLite.Entities;
using MemoLink.Data.SQLite.EntityFramework;
using MemoLink.Data.SQLite.Repositories;
using MemoLink.Data.SQLite.Services;
using MemoLink.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace MemoLink.UI.Windows
{
    public partial class Form1 : Form
    {
        private INodeService _nodeService;
        private INodeInfoService _nodeInfoService;
        private ITagService _tagService;

        public List<DTO.Tag> Tags = new List<DTO.Tag>();

        public Form1()
        {
            InitializeComponent();

            //var context = new MemoLinkDbContext(@"D:\Alexander_Symonov\Projects\MemoLink\database.sqlite");
            var context = new MemoLinkDbContext("MemoLinkContext");

            var nodeRepository = new Repository<Node>(context);
            var sourceRepository = new Repository<Source>(context);
            var tagRepository = new Repository<Tag>(context);

            _nodeService = new NodeService(nodeRepository, sourceRepository, tagRepository);
            _nodeInfoService = new NodeInfoService(nodeRepository);
            _tagService = new TagService(tagRepository);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            UpdateBundles();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                var data = treeView1.SelectedNode.Tag as DTO.NodeBase;
                int nodeId = data.Id;

                _nodeService.SaveData(
                    nodeId,
                    textBoxTitle.Text,
                    textBoxDescription.Text,
                    textBoxLink.Text,
                    Tags.Select(x => x.Name));

                data.Title = textBoxTitle.Text;
                data.Description = textBoxDescription.Text;
                data.Anchor = textBoxLink.Text;
                data.Tags = Tags.Select(x => x.Name);
               

                treeView1.SelectedNode.Text = data.Title;
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            UpdateNodeWithChildNodes(e.Node);
        }

        private void buttonCreateNode_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                int bundleId = GetBundleId(treeView1.SelectedNode);
                var parentDataSource = treeView1.SelectedNode.Tag as DTO.Node;

                _nodeService.Create(bundleId,
                    parentDataSource.Id,
                    textBoxTitle.Text,
                    textBoxDescription.Text,
                    textBoxLink.Text,
                    Tags.Select(x => x.Name));

                UpdateNodeWithChildNodes(treeView1.SelectedNode);
            }
        }


        private void UpdateBundles()
        {
            var bundles = _nodeInfoService.GetRootList();

            treeView1.Nodes.Clear();

            foreach (var bundle in bundles)
            {
                treeView1.Nodes.Add(new TreeNode(bundle.Title) {Tag = bundle});
            }
        }

        private void AppendNodes(TreeNode parent, IEnumerable<DTO.Node> nodes)
        {
            parent.Nodes.Clear();
            foreach (var node in nodes)
            {
                var newNode = new TreeNode(node.Title) {Tag = node};

                parent.Nodes.Add(newNode);
                AppendNodes(newNode, node.Nodes);
            }
        }

        private int GetBundleId(TreeNode node)
        {
            if (node == null)
            {
                throw new Exception("Empty node source");
            }

            if (node.Parent != null)
            {
                return GetBundleId(node.Parent);
            }

            var dataSource = node.Tag as DTO.Node;

            return dataSource.Id;
        }

        private void UpdateNodeWithChildNodes(TreeNode currentNode)
        {
            var dataSource = currentNode.Tag as DTO.NodeBase;
            textBoxTitle.Text = dataSource.Title;
            textBoxDescription.Text = dataSource.Description;
            textBoxLink.Text = currentNode.Parent != null ? dataSource.Anchor : dataSource.Source.Uri;

            //Tags = dataSource.Tags.Select(x => new DTO.Tag(x.Id, x.Name)).ToList();
            Tags = dataSource.Tags.Select(x => new DTO.Tag(0, x)).ToList();

            BindTags();

            linkLabelSource.Enabled = dataSource.Source != null;

            if (currentNode.Nodes.Count == 0)
            {
                var nodesList = _nodeService.GetChildNodes(dataSource.Id);
                AppendNodes(currentNode, nodesList);
            }
        }

        private void buttonCreateRoot_Click(object sender, EventArgs e)
        {
            _nodeService.CreateRoot(
                textBoxTitle.Text,
                textBoxDescription.Text,
                new DTO.Source(DTO.Source.SourceType.Default, textBoxLink.Text));

            UpdateBundles();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                _nodeService.Remove((treeView1.SelectedNode.Tag as DTO.NodeBase).Id);
                treeView1.Nodes.Remove(treeView1.SelectedNode);
            }
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            UpdateBundles();
        }

        private void buttonAddTag_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                string text = textBoxTag.Text;
                if (!string.IsNullOrEmpty(text) && !Tags.Any(x => x.Name == text))
                {
                    int tagId = _tagService.GetTagId(text);
                    Tags.Add(new DTO.Tag(tagId, text));

                    BindTags();
                }
                else
                {
                    textBoxTag.Focus();
                }
            }
        }

        private void buttonRemoveTag_Click(object sender, EventArgs e)
        {
            if (listBoxTags.SelectedItem != null)
            {
                Tags.Remove(listBoxTags.SelectedItem as DTO.Tag);

                BindTags();
            }
        }

        private void BindTags()
        {
            listBoxTags.DataSource = null;
            listBoxTags.DataSource = Tags;
            listBoxTags.DisplayMember = "Name";
            listBoxTags.ValueMember = "Id";
        }

        private void buttonSelectSource_Click(object sender, EventArgs e)
        {
            if (new Sources().ShowDialog() == DialogResult.OK)
            {

            }
        }

        private void linkLabelSource_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                var node = treeView1.SelectedNode.Tag as DTO.NodeBase;
                if (node.Source != null)
                {
                    System.Diagnostics.Process.Start(node.Source.Uri);
                }
            }
        }
    }
}