﻿using MemoLink.DTO;
using System;
using System.Windows.Forms;

namespace MemoLink.UI.Windows
{
    public partial class Sources : Form
    {
        public Source Source = null;

        public Sources()
        {
            InitializeComponent();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            var dialog = new EditSource();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                AddSource(dialog.Address);
            }
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                var dialog = new EditSource();
                dialog.Address = treeView1.SelectedNode.Text;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    treeView1.SelectedNode.Text = dialog.Address;
                }
            }
        }

        private void buttonSelect_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                Source = new Source(Source.SourceType.Default, treeView1.SelectedNode.Text);
                DialogResult = DialogResult.OK;
            }
        }

        private void AddSource(string address)
        {
            if (treeView1.SelectedNode == null)
            {
                treeView1.Nodes.Add(address);
            }
            else
            {
                treeView1.SelectedNode.Nodes.Add(address);
            }
        }
    }
}
