import axios from 'axios'

export function login ({ commit }, params) {
  return new Promise((resolve, reject) => {
    commit('authRequest')
    axios
      .post(params.url, {
        login: params.login,
        password: params.password
      })
      .then(response => {
        const token = response.data.access_token
        const user = response.data.username
        localStorage.setItem('token', token)
        localStorage.setItem('user', user)
        // Add the following line:
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
        // Trigger mutation
        commit('authSuccess', { token, user })

        resolve()
      })
      .catch(error => {
        localStorage.removeItem('token')
        reject(error)
      })
  })
}

export function logout ({ commit }) {
  return new Promise((resolve, reject) => {
    commit('authError')
    localStorage.removeItem('token')
    delete axios.defaults.headers.common['Authorization']
    resolve()
  })
}
