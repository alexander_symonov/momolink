export function authRequest (state) {
  state.status = 'loading'
}

export function authSuccess (state, data) {
  state.status = 'success'
  state.token = data.token
  state.user = data.user
}

export function authError (state) {
  state.status = 'error'
}

export function logout (state) {
  state.status = ''
  state.token = ''
}
