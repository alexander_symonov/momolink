export default {
  status: '',
  token: localStorage.getItem('token') || '',
  user: localStorage.getItem('user') || ''
}
