
const routes = [
  {
    path: '/',
    component: () => import('layouts/authenticated'),
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: '',
        component: () => import('pages/main')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('layouts/welcome'),
    children: [
      {
        path: '',
        component: () => import('pages/login')
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
