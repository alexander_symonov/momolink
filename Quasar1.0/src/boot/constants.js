export default ({ Vue }) => {
  // let baseUrl = 'https://localhost:5001'
  let baseUrl = 'http://alexanders-001-site1.itempurl.com'

  Vue.prototype.$constants = {
    ApiUrl: baseUrl + '/api',
    ApiTokenUrl: baseUrl + '/token'
  }
}
