﻿using MemoLink.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MemoLink.Data.MsSql.EntityFramework.Configuration
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id)
                .HasColumnName("ID")
                .ValueGeneratedOnAdd()
                .IsRequired();

            builder.Property(node => node.Login)
                .HasColumnName("Login")
                .IsRequired()
                .HasMaxLength(128);

            builder.Property(node => node.Password)
                .HasColumnName("Password")
                .IsRequired()
                .HasMaxLength(128);

            builder.Property(x => x.Role)
                .HasColumnName("Role")
                .IsRequired();

            builder.ToTable("User");
        }
    }
}