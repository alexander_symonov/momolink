﻿using MemoLink.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MemoLink.Data.MsSql.EntityFramework.Configuration
{
    public class SourceConfiguration : IEntityTypeConfiguration<Source>
    {
        public void Configure(EntityTypeBuilder<Source> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id)
                .HasColumnName("ID")
                .ValueGeneratedOnAdd()
                .IsRequired();

            builder.Property(x => x.Type).IsRequired();
            builder.Property(x => x.Uri)
                .HasColumnName("Link")
                .HasMaxLength(1024)
                .IsRequired();

            builder.Property(x => x.OwnerId)
                .HasColumnName("OwnerID")
                .IsRequired();

            builder.ToTable("Source");
        }
    }
}