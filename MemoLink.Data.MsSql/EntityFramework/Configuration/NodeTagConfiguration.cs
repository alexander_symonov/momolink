﻿using MemoLink.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MemoLink.Data.MsSql.EntityFramework.Configuration
{
    internal class NodeTagConfiguration : IEntityTypeConfiguration<NodeTag>
    {
        public void Configure(EntityTypeBuilder<NodeTag> builder)
        {
            builder.HasKey(x => new {x.NodeId, x.TagId});
            builder.Property(x => x.NodeId)
                .HasColumnName("NodeID")
                .IsRequired();

            builder.Property(x => x.TagId)
                .HasColumnName("TagID")
                .IsRequired();

            builder.HasOne(x => x.Tag)
                .WithMany(x => x.TagNodes);

            builder.HasOne(x => x.Node)
                .WithMany(x => x.NodeTags);

            builder.ToTable("NodeTag");
        }
    }
}