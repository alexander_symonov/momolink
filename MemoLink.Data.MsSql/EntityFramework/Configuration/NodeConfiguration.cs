﻿using MemoLink.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MemoLink.Data.MsSql.EntityFramework.Configuration
{
    public class NodeConfiguration : IEntityTypeConfiguration<Node>
    {
        public void Configure(EntityTypeBuilder<Node> builder)
        {
            builder.HasKey(node => node.Id);
            builder.Property(node => node.Id)
                .HasColumnName("ID")
                .ValueGeneratedOnAdd()
                .IsRequired();

            builder.Property(node => node.ParentId)
                .HasColumnName("ParentID");

            builder.Property(node => node.RootId)
                .HasColumnName("RootID");

            builder.Property(node => node.SourceId)
                .HasColumnName("SourceID");

            builder.Property(node => node.Anchor)
                .HasColumnName("Anchor")
                .HasMaxLength(2000);

            builder.Property(node => node.Title)
                .HasColumnName("Title")
                .IsRequired()
                .HasMaxLength(128);

            builder.Property(node => node.Description)
                .HasColumnName("Description")
                .IsRequired()
                .HasMaxLength(1024);

            builder.Property(node => node.Enabled)
                .HasColumnName("Enabled")
                .IsRequired();

            builder.Property(node => node.Shared)
                .HasColumnName("Shared")
                .IsRequired();

            builder.HasMany(node => node.Nodes)
                .WithOne(nodes => nodes.Parent);

            builder.HasMany(node => node.NodeTags)
                .WithOne(nodeTag => nodeTag.Node);

            builder.HasOne(node => node.Root);
                //.WithMany(bundle => bundle.Nodes);

            builder.HasOne(node => node.Parent);
                //.WithMany(bundle => bundle.Nodes);

            builder.HasOne(node => node.Source);

            builder.ToTable("Node");
        }
    }
}