﻿using MemoLink.Data.MsSql.EntityFramework.Configuration;
using Microsoft.EntityFrameworkCore;

namespace MemoLink.Data.MsSql.EntityFramework
{

    public class MemoLinkDbContext : DbContext
    {
        public MemoLinkDbContext(DbContextOptions<MemoLinkDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new NodeConfiguration());
            modelBuilder.ApplyConfiguration(new TagConfiguration());
            modelBuilder.ApplyConfiguration(new NodeTagConfiguration());
            modelBuilder.ApplyConfiguration(new SourceConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
