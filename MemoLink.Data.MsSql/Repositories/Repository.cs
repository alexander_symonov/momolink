﻿using MemoLink.Data.MsSql.EntityFramework;
using MemoLink.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq.Expressions;

namespace MemoLink.Data.MsSql.Repositories
{
    public class Repository<T> : ReadonlyRepository<T>, IRepository<T> where T : class
    {

        public Repository(MemoLinkDbContext context)
            : base(context)
        {
        }

        public virtual T Create(T TObject)
        {
            var newEntry = DbSet.Add(TObject);
            SaveChanges();
            return newEntry.Entity;
        }

        public virtual int Delete(T TObject)
        {
            DbSet.Remove(TObject);
            return Context.SaveChanges();
        }

        public virtual int Update(T TObject)
        {
            var entry = Context.Entry(TObject);
            DbSet.Attach(TObject);
            entry.State = EntityState.Modified;
            return Context.SaveChanges();

        }

        public virtual int Delete(Expression<Func<T, bool>> predicate)
        {
            var objects = Filter(predicate);
            foreach (var obj in objects)
                DbSet.Remove(obj);
            return Context.SaveChanges();
        }

        public int SaveChanges()
        {
            if (!shareContext)
            {
                return Context.SaveChanges();
            }
            return 0;
        }
    }
}
