﻿using MemoLink.Data.MsSql.EntityFramework;
using MemoLink.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace MemoLink.Data.MsSql.Repositories
{
    public class ReadonlyRepository<T> : IReadonlyRepository<T> where T : class
    {
        //private readonly DbSet<T> dbSet;
        protected readonly DbContext Context = null;

        public ReadonlyRepository(MemoLinkDbContext context)
        {
            //this.dbSet = context.Set<T>();
            Context = context;
        }

        protected bool shareContext = false;


        protected DbSet<T> DbSet => Context.Set<T>();

        public void Dispose()
        {
            if (shareContext && (Context != null))
                Context.Dispose();
        }

        public virtual IQueryable<T> All()
        {
            return DbSet.AsNoTracking().AsQueryable();
        }

        public virtual IQueryable<T> Filter(Expression<Func<T, bool>> predicate)
        {
            return DbSet.Where(predicate).AsQueryable<T>();
        }

        //public virtual IQueryable<T> Filter(Expression<Func<T, bool>> filter, out int total, int index = 0, int size = 50)
        //{
        //	int skipCount = index * size;
        //	var _resetSet = filter != null ? DbSet.Where(filter).AsQueryable() : DbSet.AsQueryable();
        //	_resetSet = skipCount == 0 ? _resetSet.Take(size) : _resetSet.Skip(skipCount).Take(size);
        //	total = _resetSet.Count();
        //	return _resetSet.AsQueryable();
        //}

        public bool Contains(Expression<Func<T, bool>> predicate)
        {
            return DbSet.Count(predicate) > 0;
        }

        public virtual T Find(params object[] keys)
        {
            return DbSet.Find(keys);
        }

        public virtual T Find(Expression<Func<T, bool>> predicate)
        {
            return DbSet.FirstOrDefault(predicate);
        }

        public virtual int Count => DbSet.Count();
    }
}