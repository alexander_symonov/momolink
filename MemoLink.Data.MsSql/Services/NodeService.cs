﻿using MemoLink.Domain.Entities;
using MemoLink.Domain.Repositories;
using MemoLink.Domain.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MemoLink.Data.MsSql.Services
{
    public class NodeService : INodeService
    {
        private readonly IRepository<Node> _nodeRepository;
        private readonly IRepository<Tag> _tagRepository;
        private readonly IRepository<NodeTag> _nodeTagRepository;
        private readonly IRepository<Source> _sourceRepository;

        public NodeService(
            IRepository<Node> nodeRepository,
            IRepository<Tag> tagRepository,
            IRepository<Source> sourceRepository,
            IRepository<NodeTag> nodeTagRepository
            )
        {
            _nodeRepository = nodeRepository;
            _tagRepository = tagRepository;
            _sourceRepository = sourceRepository;
            _nodeTagRepository = nodeTagRepository;
        }

        public int CreateRoot(string title, string description, DTO.Source source)
        {
            var newSource = _sourceRepository.Create(new Source()
            {
                Type = source.Type,
                Uri = source.Uri
            });

            var newItem = new Node()
            {
                Title = title,
                Description = description,
                Source = newSource,
                Enabled = true
            };

            return _nodeRepository.Create(newItem).Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootId"></param>
        /// <param name="parentId"></param>
        /// <param name="title"></param>
        /// <param name="description"></param>
        /// <param name="anchor"></param>
        /// <param name="tags"></param>
        /// <returns>ID of new node</returns>
        public int Create(int rootId, int? parentId, string title, string description, string anchor, IEnumerable<string> tags)
        {
            var rootNode = _nodeRepository.Find(rootId);

            var newItem = new Node()
            {
                Enabled = true,
                Anchor = anchor,
                Title = title,
                Description = description,
                RootId = rootId,
                SourceId = rootNode.SourceId,
                NodeTags = new List<NodeTag>()
            };

            if (parentId.HasValue)
            {
                newItem.ParentId = parentId.Value;
            }

            // Save node before saving tags. Generated ID required
            _nodeRepository.Create(newItem);

            SaveNodeTags(newItem, tags);

            return newItem.Id;
        }

        public void SaveData(int id, string title, string description, string anchor, IEnumerable<string> tags)
        {
            var node = _nodeRepository.Filter(x => x.Id == id).Include("NodeTags.Tag").Single();
            node.Title = title;
            node.Description = description;
            node.Anchor = anchor;

            SaveNodeTags(node, tags);

            _nodeRepository.SaveChanges();
        }

        private void SaveNodeTags(Node node, IEnumerable<string> tags)
        {
            var nodeTags = node.NodeTags.ToList();
            var existingTagsNames = nodeTags.Select(x => x.Tag.Name).ToList();
            var notExistingTagsNames = tags.Except(existingTagsNames).ToList();
            var existingTags = _tagRepository
                .Filter(x => notExistingTagsNames.ToList().Contains(x.Name))
                .ToList(); // Required for EF closeed connection before create new NodeTag

            // Remove absent tags
            foreach (var nodeTag in existingTagsNames.Except(tags))
            {
                node.NodeTags.Remove(nodeTags.Single(x => x.Tag.Name == nodeTag));
            }

            // Add existing tags
            foreach (var tag in existingTags)
            {
                notExistingTagsNames.Remove(tag.Name);
                _nodeTagRepository.Create(
                    new NodeTag()
                    {
                        TagId = tag.Id,
                        NodeId = node.Id
                    });
            }

            // Create and add not existing tags
            foreach (var tagName in notExistingTagsNames)
            {
                var tag = _tagRepository.Create(new Tag {Name = tagName});
                _nodeTagRepository.Create(
                    new NodeTag()
                    {
                        TagId = tag.Id,
                        NodeId = node.Id
                    });
            }
        }

        /// <summary>
        /// TODO: check to remove
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public IEnumerable<DTO.Node> GetChildNodes(int parentId)
        {
            return _nodeRepository.Filter(x => x.ParentId == parentId)
                .Select(x => new DTO.Node()
                {
                    Id = x.Id,
                    Title = x.Title,
                    Tags = x.NodeTags.Select(t => t.Tag.Name),
                    Anchor = x.Anchor,
                    Description = x.Description,
                    Source = new DTO.Source()
                    {
                        Type = x.Source.Type,
                        Uri = x.Source.Uri
                    }
                });
        }

        public IEnumerable<DTO.Node> GetBundle(int bundleId)
        {
            var nodes = GetBundleNodes(bundleId)
                .Select(x => new { x.ParentId, instance = ToDtoNode(x) })
                .ToList();
            nodes.ForEach(x =>
            {
                x.instance.Nodes = nodes.Where(sub => sub.ParentId == x.instance.Id).Select(n => n.instance).ToList();
            });

            return nodes.Where(x => x.instance.Id == bundleId).Select(x => x.instance);
        }

        public IEnumerable<DTO.Node> GetBundleData(int bundleId)
        {
            var nodes = GetBundleNodes(bundleId)
                .Select(x => ToDtoNode(x))
                .ToList();

            return nodes;
        }

        public void Remove(int id)
        {
            var node = _nodeRepository.Find(x => x.Id == id);
            node.Enabled = false;
            
            _nodeRepository.SaveChanges();
        }

        public void CloneSingle(int sourceId, int targetId)
        {
            var sourceNode = _nodeRepository.Filter(x => x.Id == sourceId).Include("NodeTags").Single();

            CloneAndSave(sourceNode, targetId);

            _nodeRepository.SaveChanges();
        }

        public void CloneDeep(int sourceId, int targetId)
        {
            var sourceNode = _nodeRepository
                .Filter(x => x.Id == sourceId)
                .Include("NodeTags")
                .Single();
            var targetNode = _nodeRepository.Find(x => x.Id == targetId);

            if (sourceNode == null)
            {
                throw new ApplicationException("Source node not found");
            }
            if (targetNode == null)
            {
                throw new ApplicationException("Target node not found");
            }
            if (!targetNode.Enabled)
            {
                throw new ApplicationException("Target node have been deleted");
            }

            var nodes = GetBundleNodes(sourceNode.RootId ?? sourceNode.Id);
            
            CloneTree(sourceNode, targetId, nodes);
        }

        public void Move(int sourceId, int targetId)
        {
            var sourceNode = _nodeRepository.Find(x => x.Id == sourceId);
            var targetNode = _nodeRepository.Find(x => x.Id == targetId);

            if (sourceNode.RootId == null)                              // Source can't be root
            {
                throw new ApplicationException("Can not move bundle root. Not supported yet.");
            }
            if (sourceNode.RootId != targetNode.RootId
                && (targetNode.RootId == null && targetNode.Id != sourceNode.RootId) // Move to root
            )
            {
                throw new ApplicationException("Nodes are in different bundles. Not supported yet.");
            }

            var bundleNodes = GetBundleNodes(sourceNode.RootId.Value);

            if (targetNode.ParentId.HasValue                            // isn't root
                && IsParentExists(targetNode, sourceId, bundleNodes))   // and target has no source as parent
            {
                throw  new ApplicationException("Target node could not bee child of source.");
            }

            sourceNode.ParentId = targetNode.Id;
            _nodeRepository.SaveChanges();
        }

        private void CloneTree(Node sourceNode, int parentId, IList<Node> bundleNodes)
        {
            var newId = CloneAndSave(sourceNode, parentId);

            foreach (var childNode in bundleNodes.Where(x => x.ParentId == sourceNode.Id))
            {
                CloneTree(childNode, newId, bundleNodes);
            }
        }

        /// <summary>
        /// Clone only in into one bucket
        /// </summary>
        /// <param name="sourceNode"></param>
        /// <param name="parentId"></param>
        /// <returns>ID of already created node</returns>
        private int CloneAndSave(Node sourceNode, int parentId)
        {
            var newItem = new Node()
            {
                ParentId = parentId,            // other parent
                Enabled = sourceNode.Enabled,
                Anchor = sourceNode.Anchor,
                Title = sourceNode.Title,
                Description = sourceNode.Description,
                RootId = sourceNode.RootId,
                SourceId = sourceNode.SourceId,
                Shared = sourceNode.Shared,
                NodeTags = sourceNode.NodeTags.Select(x => new NodeTag
                {
                    TagId = x.TagId
                }).ToList()
            };

            return _nodeRepository.Create(newItem).Id;
        }

        /// <summary>
        /// TODO: Check loop
        /// </summary>
        /// <param name="current"></param>
        /// <param name="searchId"></param>
        /// <param name="nodes"></param>
        /// <returns></returns>
        private bool IsParentExists(Node current, int searchId, IList<Node> nodes)
        {
            if (!current.ParentId.HasValue)
            {
                return false;
            }
            if (current.ParentId.Value == searchId)
            {
                return true;
            }

            var parentNode = nodes.Single(x => x.Id == current.ParentId);

            return IsParentExists(parentNode, searchId, nodes);
        }

        /// <summary>
        /// List of nodes allowed for display
        /// </summary>
        /// <param name="bundleId"></param>
        /// <returns>List of all enabled bundle nodes including Tags and Sources</returns>
        private IList<Node> GetBundleNodes(int bundleId)
        {
            return _nodeRepository
                .Filter(x => x.Enabled && x.RootId == bundleId || x.Id == bundleId)
                .Include("NodeTags.Tag")
                .Include("Source")
                .ToList();
        }

        /// <summary>
        /// Convert DB entity to DTO object
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private DTO.Node ToDtoNode(Node node)
        {
            return new DTO.Node()
            {
                Id = node.Id,
                Title = node.Title,
                ParentId = node.ParentId,
                Tags = node.NodeTags
                    .Select(x => x.Tag.Name),
                Anchor = node.Anchor,
                Description = node.Description,
                Source = new DTO.Source()
                {
                    Type = node.Source.Type,
                    Uri = node.Source.Uri,
                    OwnerId = node.Source.OwnerId
                }
            };
        }
    }
}