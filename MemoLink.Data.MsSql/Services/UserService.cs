﻿using MemoLink.Domain.Entities;
using MemoLink.Domain.Repositories;
using MemoLink.Domain.Services;
using System.Collections.Generic;
using System.Security.Claims;

namespace MemoLink.Data.MsSql.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> _userRepository;

        public UserService(IRepository<User> userRepository)
        {
            _userRepository = userRepository;
        }

        public ClaimsIdentity GetIdentity(string login, string password)
        {
            var user = _userRepository.Find(x =>
                x.Login.ToLowerInvariant() == login.ToLowerInvariant()
                && x.Password == password);

            if (user == null) return null;

            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role.ToString(), user.Role.GetType().FullName)
            };

            var claimsIdentity = new ClaimsIdentity(
                claims,
                "Token",
                ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            return claimsIdentity;
        }
    }
}
