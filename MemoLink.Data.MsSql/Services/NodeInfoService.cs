﻿using MemoLink.Domain.Entities;
using MemoLink.Domain.Repositories;
using MemoLink.Domain.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace MemoLink.Data.MsSql.Services
{
    public class NodeInfoService : INodeInfoService
    {
        private readonly IRepository<Node> _nodeRepository;

        public NodeInfoService(IRepository<Node> nodeRepository)
        {
            _nodeRepository = nodeRepository;
        }

        public IEnumerable<DTO.NodeInfo> GetRootList()
        {
            return _nodeRepository.Filter(x => x.RootId == null)
                .Include("NodeTags.Tag")
                .Include("Nodes")
                .Include("Source")
                //.Include("Anchor")
                .ToList()
                .Select(ToDto);
        }

        public IEnumerable<DTO.NodeInfo> GetChildsInfo(int nodeId)
        {
            return _nodeRepository.Filter(x => x.ParentId == nodeId)
                .Include("NodeTags.Tag")
                .Include("Nodes")
                .Include("Source")
                //.Include("Anchor")
                .ToList()
                .Select(ToDto);
        }

        private DTO.NodeInfo ToDto(Node node)
        {
            return new DTO.NodeInfo()
            {
                Id = node.Id,
                Title = node.Title,
                NodesCount = node.Nodes.Count(),
                Tags = node.NodeTags
                    .Select(x => x.Tag.Name),
                Anchor = node.Anchor,
                Description = node.Description,
                Source = new DTO.Source()
                {
                    Type = node.Source.Type,
                    Uri = node.Source.Uri
                }
            };
        }
    }
}