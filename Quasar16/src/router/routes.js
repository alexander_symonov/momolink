
export default [
  {
    path: '/',
    component: () => import('layouts/main'),
    children: [
      {
        path: '',
        component: () => import('pages/Main')
      }
    ]
  },
  {
    path: '/test',
    component: () => import('layouts/main'),
    children: [
      {
        path: '',
        component: () => import('pages/test')
      }
    ]
  },
  {
    path: '/testauto',
    component: () => import('layouts/main'),
    children: [
      {
        path: '',
        component: () => import('pages/testauto')
      }
    ]
  },
  {
    path: '/index',
    component: () => import('layouts/default'),
    children: [
      {
        path: '',
        component: () => import('pages/index')
      }
    ]
  },
  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
