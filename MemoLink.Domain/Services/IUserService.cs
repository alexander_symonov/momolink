﻿using System.Security.Claims;

namespace MemoLink.Domain.Services
{
    public interface IUserService
    {
        ClaimsIdentity GetIdentity(string login, string password);
    }
}
