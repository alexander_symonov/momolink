﻿using System.Collections.Generic;

namespace MemoLink.Domain.Services
{
    public interface ITagService
    {
        int GetTagId(string name);
        IEnumerable<int> GetTagsIds(List<string> names);
        int Clear();
    }
}