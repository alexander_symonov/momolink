﻿using System.Collections.Generic;

namespace MemoLink.Domain.Services
{
    public interface INodeInfoService
    {
        IEnumerable<DTO.NodeInfo> GetRootList();
        IEnumerable<DTO.NodeInfo> GetChildsInfo(int nodeId);
    }
}
