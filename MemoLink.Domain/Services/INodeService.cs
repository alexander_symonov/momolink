﻿using System.Collections.Generic;

namespace MemoLink.Domain.Services
{
    public interface INodeService
    {
        int CreateRoot(string title, string description, DTO.Source source);
        int Create(int rootId, int? parentId, string title, string description, string anchor, IEnumerable<string> tags);
        void SaveData(int id, string title, string description, string anchor, IEnumerable<string> tags);
        void Remove(int id);
        IEnumerable<DTO.Node> GetChildNodes(int parentId);
        IEnumerable<DTO.Node> GetBundleData(int bundleId);
        //        DTO.Node GetBundle(int bundleId);
        IEnumerable<DTO.Node> GetBundle(int bundleId);
        void CloneSingle(int sourceId, int targetId);
        void CloneDeep(int sourceId, int targetId);
        void Move(int sourceId, int targetId);
    }
}