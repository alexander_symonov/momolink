﻿namespace MemoLink.Domain.Entities
{
    public class Source
    {
        public int Id { get; set; }
        public DTO.Source.SourceType Type { get; set; }
        public string Uri { get; set; }
        public int OwnerId { get; set; }
    }
}