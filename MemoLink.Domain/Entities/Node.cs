﻿using System.Collections.Generic;

namespace MemoLink.Domain.Entities
{
    public class Node
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Anchor { get; set; }

        public bool Enabled { get; set; }

        public bool Shared { get; set; }

        public int? RootId { get; set; }

        public int? ParentId { get; set; }

        public int? SourceId { get; set; }

        public Source Source { get; set; }
        
        public Node Root { get; set; }

        public Node Parent { get; set; }

        public ICollection<Node> Nodes { get; set; }

        public ICollection<NodeTag> NodeTags { get; set; }

        //public Node()
        //{
        //    //Tags = new List<NodeTag>();
        //    //this.Source = new Source();
        //}
    }
}