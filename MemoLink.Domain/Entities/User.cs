﻿namespace MemoLink.Domain.Entities
{
    public enum UserRoles
    {
        Guest = 1,
        Default = 2,
        Admin = 7
    }

    public class User
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public UserRoles Role { get; set; }
    }
}