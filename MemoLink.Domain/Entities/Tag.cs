﻿using System.Collections.Generic;

namespace MemoLink.Domain.Entities
{
    public class Tag
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<NodeTag> TagNodes { get; set; }

        public Tag() { }

        public Tag(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}