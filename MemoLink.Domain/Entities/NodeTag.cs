﻿using System.Collections.Generic;

namespace MemoLink.Domain.Entities
{
    public class NodeTag
    {
        public int NodeId { get; set; }

        public int TagId { get; set; }

        public Node Node { get; set; }

        public Tag Tag { get; set; }
    }
}