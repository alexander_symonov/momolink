﻿namespace MemoLink.WebApi.Models.NodeModels
{
    public class CreateNodeModel : NodeModelBase
    {
        public int RootId { get; set; }
        public int? ParentId { get; set; }

        public override bool IsValid()
        {
            return base.IsValid() && RootId != 0;
        }
    }
}