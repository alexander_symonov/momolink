﻿using MemoLink.Domain.Services;
using MemoLink.DTO;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using MemoLink.WebApi.Models.BundleModels;
using Microsoft.AspNetCore.Authorization;

namespace MemoLink.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [EnableCors("Internal")]
    [ApiController]
    public class BundleController : ControllerBase
    {
        private readonly INodeInfoService _nodeInfoService;
        private readonly INodeService _nodeService;

        public BundleController(INodeInfoService nodeInfoService, INodeService nodeService)
        {
            _nodeInfoService = nodeInfoService;
            _nodeService = nodeService;
        }

        // GET: api/Bundle
        public IEnumerable<NodeInfo> Get()
        {
            return _nodeInfoService.GetRootList();
        }

        
        // GET: api/Bundle/5
        [HttpGet("Tree/{id}", Name = "Tree")]
        public IEnumerable<Node> Tree(int id)
        {
            return _nodeService.GetBundle(id);
        }

        
        // POST: api/Bundle
        [HttpPost]
        public void Post([FromBody] CreateBundleModel model)
        {
            _nodeService.CreateRoot(model.Title, model.Description, model.Source);
        }

        /*
        // PUT: api/Bundle/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
        */
    }
}
