﻿using MemoLink.Domain.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace MemoLink.WebApi.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("Internal")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly INodeService _nodeService;

        public TestController(INodeService nodeService)
        {
            _nodeService = nodeService;
        }

        [HttpGet]
        public JsonResult Get()
        {
            var result = _nodeService.GetBundleData(1);
            return new JsonResult(result);
        }
    }
}