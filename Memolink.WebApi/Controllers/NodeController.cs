﻿using MemoLink.Domain.Services;
using MemoLink.WebApi.Models.NodeModels;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel;
using Microsoft.AspNetCore.Authorization;

namespace MemoLink.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [EnableCors("Internal")]
    [ApiController]
    public class NodeController : ControllerBase
    {
        private INodeService _nodeService;

        public NodeController(INodeService nodeService)
        {
            _nodeService = nodeService;
        }

        /*
        // GET: api/Node
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Node/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }*/

        // PUT: api/Node/5
        public int Put([FromBody] CreateNodeModel model)
        {
            if (!model.IsValid())
            {
                throw new ArgumentOutOfRangeException();
            }

            return _nodeService.Create(
                model.RootId,
                model.ParentId,
                model.Title,
                model.Description ?? string.Empty,
                model.Anchor ?? string.Empty,
                model.Tags);
        }

        // DELETE: api/Node/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _nodeService.Remove(id);
        }

        [HttpPost]
        public void Post([FromBody] UpdateNodeModel model)
        {
            if (!model.IsValid())
            {
                throw new ArgumentOutOfRangeException();
            }

            _nodeService.SaveData(
                model.Id,
                model.Title,
                model.Description ?? string.Empty,
                model.Anchor,
                model.Tags);
        }

        // POST: api/Node/Clone
        [HttpPost]
        [Route("Clone")]
        public void Clone([FromBody] CloneNodeModel model)
        {
            if (!model.IsValid())
            {
                throw new ArgumentOutOfRangeException();
            }

            switch (model.Type)
            {
                case CloneType.Cut:
                    _nodeService.Move(model.SourceId, model.TargetId);
                    break;
                case CloneType.Single:
                    _nodeService.CloneSingle(model.SourceId, model.TargetId);
                    break;
                case CloneType.Full:
                    _nodeService.CloneDeep(model.SourceId, model.TargetId);
                    break;
                default:
                    throw new InvalidEnumArgumentException(nameof(model.Type), (int)model.Type, model.Type.GetType());
            }
        }
    }
}
