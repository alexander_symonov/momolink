﻿using MemoLink.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Memolink.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SourceController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<Source.SourceType> Types()
        {
            return Enum.GetValues(typeof(Source.SourceType)).Cast<Source.SourceType>();
        }

        /*
        // GET: api/Source
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Source/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Source
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Source/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }*/
    }
}
