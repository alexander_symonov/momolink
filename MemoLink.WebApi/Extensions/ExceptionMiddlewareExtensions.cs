﻿using System.Data.SqlClient;
using System.IO;
using MemoLink.WebApi.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using System.Net;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace MemoLink.WebApi.Extensions
{
    public static class ExceptionMiddlewareExtensions
    {
        public static void ConfigureExceptionHandler(this IApplicationBuilder app /*, ILoggerManager logger*/)
        {
            // Dont know why, but XHR responce body is always empty !

            /*
            app.UseExceptionHandler(
                new ExceptionHandlerOptions
                {
                    ExceptionHandler = async context =>
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                        context.Response.ContentType = "application/json";

                        var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                        if (contextFeature != null)
                        {
                            //logger.LogError($"Something went wrong: {contextFeature.Error}");
                            string message = "!!!!!!";
                            if (contextFeature.Error is DbUpdateException
                                && contextFeature.Error.InnerException is SqlException sqlEx
                                && sqlEx.ErrorCode == -2146232060) // Truncate exception code 
                            {
                                message = "Internal Server Error. Too long field";
                            }
                            var errObject = new ErrorDetails()
                            {
                                StatusCode = context.Response.StatusCode,
                                Message = message
                            };
                            var result = JsonConvert.SerializeObject(errObject);
                            await context.Response.WriteAsync(result);

                        //    using (var writer = new StreamWriter(context.Response.Body))
                        //    {
                        //        new JsonSerializer().Serialize(writer, errObject);
                        //        await writer.FlushAsync().ConfigureAwait(false);
                        //    }
                        }
                    }
                });
*/

            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                    context.Response.ContentType = "application/json";

                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (contextFeature != null)
                    {
                        //logger.LogError($"Something went wrong: {contextFeature.Error}");
                        string message = "Internal Server  Error.";
                        if (contextFeature.Error is DbUpdateException
                            && contextFeature.Error.InnerException is SqlException sqlEx
                            && sqlEx.ErrorCode == -2146232060) // Truncate exception code 
                        {
                            message = "Internal Server Error. Too long field";
                        }

                        var result = JsonConvert.SerializeObject(new ErrorDetails()
                        {
                            StatusCode = context.Response.StatusCode,
                            Message = message
                        });

                        await context.Response.WriteAsync(result);
                    }
                });
            });
        }
    }
}
