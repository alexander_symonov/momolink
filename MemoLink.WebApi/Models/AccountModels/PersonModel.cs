﻿namespace MemoLink.WebApi.Models.AccountModels
{
    public class PersonModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }
}
