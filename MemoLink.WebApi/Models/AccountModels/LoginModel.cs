﻿using System.ComponentModel.DataAnnotations;

namespace MemoLink.WebApi.Models.AccountModels
{
    public class LoginModel
    {
        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
