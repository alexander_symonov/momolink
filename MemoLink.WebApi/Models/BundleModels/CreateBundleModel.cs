﻿using MemoLink.DTO;

namespace MemoLink.WebApi.Models.BundleModels
{
    public class CreateBundleModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public Source Source { get; set; }
    }
}
