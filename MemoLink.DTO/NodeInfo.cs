﻿namespace MemoLink.DTO
{
    public class NodeInfo : NodeBase
    {
        public int NodesCount { get; set; }
    }
}