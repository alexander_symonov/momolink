﻿using System.Collections.Generic;

namespace MemoLink.DTO
{
    public class NodeBase
    {
        public int Id { get; set; }

        public int? ParentId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Anchor { get; set; }

        public Source Source { get; set; }

        //public IEnumerable<Tag> Tags { get; set; }
        public IEnumerable<string> Tags { get; set; }

        public bool IsSourceOwner
        {
            get
            {
                return this.Id == Source.OwnerId;
            }
        }

        public NodeBase()
        {
            Tags = new List<string>();
        }
    }
}