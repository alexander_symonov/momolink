﻿namespace MemoLink.DTO
{
    public class Source
    {
        public enum SourceType : byte
        {
            Default = 1,
            None = 2,
            Html = 3,
            Pdf = 4
        }

        public SourceType Type { get; set; }

        public string Uri { get; set; }

        public int OwnerId { get; set; }

        public Source()
        {
        }

        public Source(SourceType type, string uri)
        {
            Type = type;
            Uri = uri;
        }
    }
}