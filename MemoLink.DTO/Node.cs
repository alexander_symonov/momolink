﻿using System.Collections.Generic;
using System.Linq;

namespace MemoLink.DTO
{
    public class Node : NodeBase
    {
        public IEnumerable<Node> Nodes { get; set; }

        public Node()
        {
            Nodes = new List<Node>();
        }
        public bool HasChildren
        {
            get { return Nodes.Count() > 0; }
        }
    }
}